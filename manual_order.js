const Binance = require('node-binance-api');
const readline = require("readline");
const fs = require('fs')
let jsonData = JSON.parse(fs.readFileSync('./exchangeInfo.json', 'utf-8'));


const binance = new Binance().options({
  APIKEY: 'APIKEY',
  APISECRET: 'APISECRET'
});

global.filters = {};


function loadMinValues(jsonData) {
  data = jsonData;
  let minimums = {};

  for ( let obj of data.symbols ) {
    let filters = {status: obj.status};
    for ( let filter of obj.filters ) {
      if ( filter.filterType == "MIN_NOTIONAL" ) {
        filters.minNotional = filter.minNotional;
      } else if ( filter.filterType == "PRICE_FILTER" ) {
        filters.minPrice = filter.minPrice;
        filters.maxPrice = filter.maxPrice;
        filters.tickSize = filter.tickSize;
      } else if ( filter.filterType == "LOT_SIZE" ) {
        filters.stepSize = filter.stepSize;
        filters.minQty = filter.minQty;
        filters.maxQty = filter.maxQty;
      }
    }
    filters.orderTypes = obj.orderTypes;
    filters.icebergAllowed = obj.icebergAllowed;
    minimums[obj.symbol] = filters;
  }
  // console.log(minimums);
  global.filters = minimums;
}
loadMinValues(jsonData);



const BTC_TO_INVEST = 0.02;
let symbol;

function askQuestion(query) {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });

    return new Promise(resolve => rl.question(query, ans => {
        rl.close();
        resolve(ans);
    }))
}

async function startOrder() {
  try {
    let symbol = await askQuestion("Enter the coin\n");
    symbol = symbol.toUpperCase()+'BTC';
    let price = await binance.prices(symbol);
    price = price[symbol]

    let minQty = filters[symbol].minQty;
    let minNotional = filters[symbol].minNotional;
    let stepSize = filters[symbol].stepSize;

    let amount = BTC_TO_INVEST/price;

    if ( amount < minQty ) amount = minQty;
    // Set minimum order amount with minNotional
    if ( price * amount < minNotional ) {
      amount = minNotional / price;
    }
    // Round to stepSize
    amount = binance.roundStep(amount, stepSize);

    binance.buy(symbol, amount, price, {type:'LIMIT'}, (error, response) => {
      console.log("Limit Buy response", response);
      console.log("order id: " + response.orderId);
      console.log(error);
      console.log(price,amount);
    });


  } catch(e) {
    console.log(e);
  }
}

startOrder()







