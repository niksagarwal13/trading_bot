process.env.NTBA_FIX_319 = 1;
const controller = require('./controller');
const { exec } = require("child_process");
const order = require('./order');

controller({
  'checkers': [
    {
      'name': 'Strategy 1',
      'condition': (lastTicker, currentTicker) => {
        let perc_change = 0.05
        let time_change = 5000
        let ticker_surge = parseFloat(currentTicker.c) >= (parseFloat(lastTicker.c) * (1 + perc_change));
        let time_surge = currentTicker.E - lastTicker.E <= time_change;
        let high_price_surge = parseFloat(currentTicker.c) > parseFloat(lastTicker.h);
        let opening_surge = parseFloat(currentTicker.P) >= 5;

        // console.log(currentTicker.s, (new Date(currentTicker.E)).toLocaleString(), ticker_surge, time_surge, high_price_surge, opening_surge);
        return (ticker_surge && high_price_surge && time_surge && opening_surge);
      },
      'update': (lastTicker, currentTicker) => {
        return currentTicker
      },
      'placeOrder': ticker => {
        order(ticker);
      },
      'do': data => {
        data.filteredTickers.forEach((ticker) => {
          let message = `Strategy No: ${data.name} \n Symbol: ${ticker.lastTicker.s} \n Last close: ${ticker.lastTicker.c} \n Current close: ${ticker.currentTicker.c} \n Last Ticker Time: ${(new Date(ticker.lastTicker.E)).toLocaleString()} \n Current Ticker Time: ${(new Date(ticker.currentTicker.E)).toLocaleString()}`

          console.log(message);
        })
      }
    },
    {
      'name': 'Strategy 2',
      'condition': (lastTicker, currentTicker) => {
        let perc_change = 0.05
        let time_change = 5000
        let ticker_surge = parseFloat(currentTicker.c) >= (parseFloat(lastTicker.c) * (1 + perc_change));
        let time_surge = currentTicker.E - lastTicker.E <= time_change;
        let high_price_surge = parseFloat(currentTicker.c) > parseFloat(lastTicker.h);
        let opening_surge = parseFloat(currentTicker.P) >= 5;

        // console.log(currentTicker.s, ticker_surge, time_surge, high_price_surge, opening_surge, min_price);
        return (ticker_surge && high_price_surge && time_surge && opening_surge);
      },
      'update': (lastTicker, currentTicker) => {
        return currentTicker
      },
      'placeOrder': ticker => {
        order(ticker);
      },
      'do': data => {
        data.filteredTickers.forEach((ticker) => {
          let message = `Strategy No: ${data.name} \n Symbol: ${ticker.lastTicker.s} \n Last close: ${ticker.lastTicker.c} \n Current close: ${ticker.currentTicker.c} \n Last Ticker Time: ${(new Date(ticker.lastTicker.E)).toLocaleString()} \n Current Ticker Time: ${(new Date(ticker.currentTicker.E)).toLocaleString()}`

          console.log(message);
        })
      }
    },
    {
      'name': 'Strategy 3',
      'condition': (lastTicker, currentTicker) => {
        let perc_change = 0.1
        let time_change = 5000
        let ticker_surge = parseFloat(currentTicker.c) >= (parseFloat(lastTicker.c) * (1 + perc_change));
        let time_surge = currentTicker.E - lastTicker.E <= time_change;
        let high_price_surge = parseFloat(currentTicker.c) > parseFloat(lastTicker.h);
        // let opening_surge = parseFloat(currentTicker.P) >= 5;

        // console.log(currentTicker.s, ticker_surge, time_surge, high_price_surge, opening_surge);
        return (ticker_surge && time_surge && high_price_surge && opening_surge);
      },
      'update': (lastTicker, currentTicker) => {
        return currentTicker
      },
      'placeOrder': ticker => {
        order(ticker);
      },
      'do': data => {
        data.filteredTickers.forEach((ticker) => {
          let message = `Strategy No: ${data.name} \n Symbol: ${ticker.lastTicker.s} \n Last close: ${ticker.lastTicker.c} \n Current close: ${ticker.currentTicker.c} \n Last Ticker Time: ${(new Date(ticker.lastTicker.E)).toLocaleString()} \n Current Ticker Time: ${(new Date(ticker.currentTicker.E)).toLocaleString()}`

          console.log(message);
        })
      }
    },
    {
      'name': 'Strategy 4',
      'condition': (lastTicker, currentTicker) => {
        let perc_change = 0.1
        let time_change = 10000
        let ticker_surge = parseFloat(currentTicker.c) >= (parseFloat(lastTicker.c) * (1 + perc_change));
        let time_surge = currentTicker.E - lastTicker.E <= time_change;
        let high_price_surge = parseFloat(currentTicker.c) > parseFloat(lastTicker.h);
        let opening_surge = parseFloat(currentTicker.P) >= 5;

        // console.log(currentTicker.s, ticker_surge, time_surge, high_price_surge, opening_surge);
        return (ticker_surge && time_surge && high_price_surge && opening_surge);
      },
      'update': (lastTicker, currentTicker) => {
        return currentTicker
      },
      'placeOrder': ticker => {
        order(ticker);
      },
      'do': data => {
        data.filteredTickers.forEach((ticker) => {
          let message = `Strategy No: ${data.name} \n Symbol: ${ticker.lastTicker.s} \n Last close: ${ticker.lastTicker.c} \n Current close: ${ticker.currentTicker.c} \n Last Ticker Time: ${(new Date(ticker.lastTicker.E)).toLocaleString()} \n Current Ticker Time: ${(new Date(ticker.currentTicker.E)).toLocaleString()}`

          console.log(message);
        })
      }
    }
  ]
})
