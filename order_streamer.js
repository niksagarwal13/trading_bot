const Binance = require('node-binance-api');
const fs = require('fs');
let jsonData = JSON.parse(fs.readFileSync('./exchangeInfo.json', 'utf-8'));
const binance = new Binance().options({
  APIKEY: 'APIKEY',
  APISECRET: 'APISECRET'
});
global.filters = {};

function loadMinValues(jsonData) {
  data = jsonData;
  let minimums = {};

  for ( let obj of data.symbols ) {
    let filters = {status: obj.status};
    for ( let filter of obj.filters ) {
      if ( filter.filterType == "MIN_NOTIONAL" ) {
        filters.minNotional = filter.minNotional;
      } else if ( filter.filterType == "PRICE_FILTER" ) {
        filters.minPrice = filter.minPrice;
        filters.maxPrice = filter.maxPrice;
        filters.tickSize = filter.tickSize;
      } else if ( filter.filterType == "LOT_SIZE" ) {
        filters.stepSize = filter.stepSize;
        filters.minQty = filter.minQty;
        filters.maxQty = filter.maxQty;
      }
    }
    filters.orderTypes = obj.orderTypes;
    filters.icebergAllowed = obj.icebergAllowed;
    minimums[obj.symbol] = filters;
  }
  global.filters = minimums;
}
loadMinValues(jsonData);
const TP_PERCENT = 0.40;

async function seeIfYouWantToCreateSellOrder(data) {
  let coin = data.s;
  let precision = filters[coin].tickSize.split(".")[1].split("1")[0].length+1;
  let buy_price = data.p;
  let stop_limit_price = (buy_price * (1 - 0.05)).toFixed(precision);
  let selling_price = (buy_price * (1 + TP_PERCENT)).toFixed(precision);
  let quantity = data.q;
  console.log(buy_price, coin, quantity);
  binance.sell(coin, quantity, selling_price, { type:'OCO' , stopLimitPrice: stop_limit_price, stopPrice: stop_limit_price }, (error, response) => {
    console.log(error);
    console.log(response);
    console.log(buy_price, coin, quantity, stop_limit_price, selling_price);
  })
}

function balance_update(data) {
  // console.log("Balance Update");
  // for ( let obj of data.B ) {
  //   let { a:asset, f:available, l:onOrder } = obj;
  //   if ( available == "0.00000000" ) continue;
  //   console.log(asset+"\tavailable: "+available+" ("+onOrder+" on order)");
  // }
}
async function execution_update(data) {
  let { x:executionType, s:symbol, p:price, q:quantity, S:side, o:orderType, i:orderId, X:orderStatus } = data;
  if ( orderStatus == "FILLED" && side == "BUY") {
    if ( orderStatus == "REJECTED" ) {
      console.log("Order Failed! Reason: "+data.r);
    }
    await seeIfYouWantToCreateSellOrder(data);
    console.log(symbol+" "+side+" "+orderType+" ORDER #"+orderId+" ("+orderStatus+")");
    console.log("..price: "+price+", quantity: "+quantity);
    
    return;
  }
  console.log(symbol+"\t"+side+" "+executionType+" "+orderType+" ORDER #"+orderId);
}
binance.websockets.userData(balance_update, execution_update);
