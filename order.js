const Binance = require('node-binance-api');
const fs = require('fs');
let jsonData = JSON.parse(fs.readFileSync('./exchangeInfo.json', 'utf-8'));
const binance = new Binance().options({
  APIKEY: 'INSERT YOUR APIKEY HERE',
  APISECRET: 'INSERT YOUR APISECRET HERE'
});

global.orderPlaced = false;
global.filters = {};

function loadMinValues(jsonData) {
  data = jsonData;
  let minimums = {};

  for ( let obj of data.symbols ) {
    let filters = {status: obj.status};
    for ( let filter of obj.filters ) {
      if ( filter.filterType == "MIN_NOTIONAL" ) {
        filters.minNotional = filter.minNotional;
      } else if ( filter.filterType == "PRICE_FILTER" ) {
        filters.minPrice = filter.minPrice;
        filters.maxPrice = filter.maxPrice;
        filters.tickSize = filter.tickSize;
      } else if ( filter.filterType == "LOT_SIZE" ) {
        filters.stepSize = filter.stepSize;
        filters.minQty = filter.minQty;
        filters.maxQty = filter.maxQty;
      }
    }
    filters.orderTypes = obj.orderTypes;
    filters.icebergAllowed = obj.icebergAllowed;
    minimums[obj.symbol] = filters;
  }
  global.filters = minimums;
}
const BTC_TO_INVEST = 0.04;

let order = (ticker) => {
  if (orderPlaced) { return true; }
  orderPlaced = true
  loadMinValues(jsonData);
  let precision = filters[ticker.currentTicker.s].tickSize.split(".")[1].split("1")[0].length+1
  let minQty = filters[ticker.currentTicker.s].minQty;
  let minNotional = filters[ticker.currentTicker.s].minNotional;
  let stepSize = filters[ticker.currentTicker.s].stepSize;
  let price = ticker.currentTicker.c;
  let amount = BTC_TO_INVEST/price;
  if ( amount < minQty ) amount = minQty;
  if ( price * amount < minNotional ) {
    amount = minNotional / price;
  }
  amount = binance.roundStep(amount, stepSize);
  price = parseFloat(price).toFixed(precision);
  console.log(ticker.currentTicker.s, amount, price);
  console.log('placing order');
  binance.buy(ticker.currentTicker.s, amount, price , {type:'LIMIT'}, (error, response) => {
    console.log("Limit Buy response", response);
    console.log("order id: " + response.orderId);
    console.log(error);
    console.log(ticker.currentTicker.s);
  });
}

module.exports = order
