This is trading bot for binance.

This is a terminal based javascript bot which is used for trading in crypto currencies in binance.com.
This bot detects any type of sudden price surge in any of the coins listed in binance and places a buy order at the exact time and after the buy order is filled, it also places a OCO sell order in a required profit percentage with stop loss.

Dependencies:
node-binance-api (npm install node-binance-api)


Clone the repository in your local machine, insert the api key and secret key of your binance account to make this bot work.

First run the file index.js(node index.js) in one terminal and then run the file order_streamer.js(node order_streamer.js) in another terminal.

index.js file is used to detect the price surge and place the order in the same coin whereas order_streamer.js places the sell order when the buy order is filled.